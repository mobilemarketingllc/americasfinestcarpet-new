<?php


// Area Rug Gallery Images
function arearugGallery(){
    $images = array('https://s3-us-west-2.amazonaws.com/rugs.shop/karastan-rug39600_21004_096132_rm01.jpeg',
    'https://s3-us-west-2.amazonaws.com/rugs.shop/karastan_spicemarket_room.jpg',
    'https://uca50882c99ba8a2860406828188.previews.dropboxusercontent.com/p/thumb/AAYrZgEqNjW10QlsL6znTRSpWOTvrXMa8gch4K7a6cSP7GxcC2eTEJVtIF1YLQqQ4MtA17ZDECRdsIqFHPiVbJPMBvtxxMrag_0BFVQdXjJBeF5kX4OabIafJfKInSQ8aCLUePfpex5YgzR8FaoADB16SDDWXdq-m056RHCYlr3pjJwaM_8ydNpvUcLxff1iRxxJBy6Me8Z6OQD8wndk-VlQa-481zCSy6OxNl1FIu3Uqx5nrGsNsWYbcmbZxwWZD8ohzAky4nq7GzKg-dsZfaGLy3lJG4AjAqksCmEAXItc6OPGDy0jcmO7oH8IvziD5MwSZxD07I93IrrahuIb9PJyZ7HyzuRsWhZSo6mCB_B6PXJyU5ryoSu-Bj6t4uk7wkQ/p.jpeg?fv_content=true&size_mode=5',
    'https://uc2e3078cf6c3e0d545bd957cf31.previews.dropboxusercontent.com/p/thumb/AAZE1uy_l2MCT3cYoFn745snlAv2OVTcOQInTt3_Sa064IOp-t24t0wguXQrWg11RY0HxHKihO5Zl9gN98LiLOJqLwR6LQHWYmkPwYSDZWlBRZGQasdkPwBOAB_pIvo-XeyzeaA1KMm3KqMTT85Ma9sMJ9qVNYIvNXy7AOMDUHJePCRa_KhH23jDbVWPxi2ZvFAdhZhitzPZPn8WPdVuixr_dOCe4jE6iLdhMqJk6TFMD0kORizZMK1C9gO4N3TZS7kXZAhKmajOPf2D2kpmG-YzRz2cNXZslRP53-0GQ-j8_YvQlojvSwPf9M5VME95731mD9nnCVKgY9UngYwm7_LCa7HcHsqyDwlDLa1fI_TS-WnAby_24RKH4oWZ7_cXv5o/p.jpeg?fv_content=true&size_mode=5',
    'https://ucba04b34212dce332a9933719e8.previews.dropboxusercontent.com/p/thumb/AAas2RqP6kRooymq7cVEh0uQ1FZKLiGQZAeAEaEZfo3G53bWMj27cQq9-6gFuCTOdN-aTmlPkbH0VIqb7dNT6TeSChXlUn7R2wpMAP7_Qf_wcYG5DRupL19-HWXOlXgtsJGUiND5cZgNqyNFpEsgJvwyn8Gr9sD96G5xwDpfvivGZIm3xfhhLXpVqDB5KPCBSjDgMLkweyt5dAnRdXOINKewYIrM_sQ4pC3pnuDvMWjvKbozCNozeC1l8HzDCun0Rmsf39lhsbZxDtm363OumjIHg7TNXqrU_UfqTASbt0oFU2IzBWYqZ_OMThjoZrDy6eB5E30VE-4wLYw8KWDmqHNKU9mACUK32EMNer3p3KJ50Gc3CWoqTo2CLbpF2vG1MxY/p.jpeg?fv_content=true&size_mode=5',
    'https://ucdc27270fea2babaaac19b88061.previews.dropboxusercontent.com/p/thumb/AAbOn4adlb_8IwURv-llTeymNwOvJWYNAaP0eDKdTFtDwTIoNLCEdA-XRPeKHpJrubOX4c3Z0t_s1nI2opzeKWsrjevVbHisdMOs7pqF0ntBa8bNE0Nc0hDLy0R0BeFXFGphX3VXXezjYZ_c7je8q_60KRiq9a2noTZKaaS_FaP1lAYyq2ohtlH88vjY-sdBAjH2HAfieAnjwRlsVJjtGrB8WUCzj2GOjCKg_9lU_usUQ3BnlbURVJRqQRfDuJCT7OtMWrRpCCjWJkZ1dw59PMpiAdZegMrVkv6-Ij7-X_l7z0o1aEj7Ijbsdzr6dFH7yGTaJwOH0NllL5Id3cGsFN3iLn6TE4rZfFNVOd08DQCAT1JK-5DlVkLl8fhbwlWttIU/p.jpeg?fv_content=true&size_mode=5',
    'https://ucaec0b4b093dcbae9802224b95b.previews.dropboxusercontent.com/p/thumb/AAasCTLUctRx-y2UclCA76Q3V67KjRic917MI19JJ9JSAftfDMh64golgYPJjyaxjpf3TUZ5kO_OikcOH3Lszk0WpfRLbIZcxOPVU05bEKy55ATYlwtHy6wAKuIBs-ho7fSb135-HVL1eRA5TZ8Nu2z92djfBYSbK503EjxfCeIUmwRpKJIa-jtM9941hE3giCEcnCkeWKMK92PWx5t1CvbHbaxUAlCLoaIuZB3EeDZFQ7T3p64OSdqXFUj_W26gnP5Yv34zMe7CIc3axsyVEiWZJB-3VFEwfDmXklQLR2bwtxwxe4dxTqYIXlfArWSHhkACl0IMWeqi6eM-XwYVEE3PCw6jmF5D5tzAQxUnh5_QBH-owh3aLZNr_rGjkt_lOHM/p.jpeg?fv_content=true&size_mode=5',
    'https://uc67211dd60046004e95f15e0495.previews.dropboxusercontent.com/p/thumb/AAZdC9zFI5NgLZAdAChU-wCguOAeO6yNa2lKcljDwWwsgBGdRstrFr7PQuaUUssEm0VFEB05XO5wptwcwEbURIfZIWOE417C5PrXQw6xrKPTIAQ0Br0WkDQItxb4s_u3K-DLWvKWLES_HT4rctxKZMeGBLCu2BPgNBIlxnFG5yTYuZrqoahw9yb3aDbd3Cjk2Cr8nF2YfDc-wvoRkb8cgGkqkvirVeRECWpr5iu16cynEbQ_nm6mOpBxIn42Ox74ZGnRWTvzxJgqA9ogbyO5HbOoxneYrY-wD-CzreQdYTEWMc-qD4R-QUJVyvC418Z2ypXkQomhxhQ7rK9Yqd_DJOpE8fdbi1CH2dWRHaGYZXE1ftGTYYzus2IjzYsyP6akwEM/p.jpeg?fv_content=true&size_mode=5',
    'https://uca876d78c774cebc7e8a922bd2f.previews.dropboxusercontent.com/p/thumb/AAYBSM1QFcYvAoEYKIZfwg4ELQhz9k6bq81Ucdixcym-zeAAiQ3__Kl0rAQAtHzzClrD9UDn-UGh53bf_9hy8EVWV6uq-z1rDQL58UKng4i_bio-X0TqRdPQMhqDGpDkOSFqwhYm2KIIkGsKM_PbSkF2uf40Vn-PzrKjIxVQWgkNPoNQaFW9WL2hiPHo4If9Rm2G9Km75mCs0ew1liW1t1fiN8_Pe54l-VjkMOpPkntKXI3aQJsc2ho0d3L7aa2cAL4ADBRlUtweiYgyqVWlh8NW-UC524SqKBFDUpWAnxfwc5ZvYQNeOCx27BwXhxX-9gfqYC0suI0tennLIplK6P8d3qDSIPluQHfShtmXM0YiZvMXXyY4TojpbW3S2cx6SZM/p.jpeg?fv_content=true&size_mode=5',
    'https://ucc2edb556adee3cdcab67e2fd3a.previews.dropboxusercontent.com/p/thumb/AAbIJ8tW2XcmxqiSJZ9BVk8n7s9cLf7W6PXi0xW8uLnzWk0rx_HShpmvs9r7es4ec2irQocLsxP1-MRVngLJEPTv1F-Ng9PQ9fzWKCs-OLKvTmqiXBqpC0F-1tyg6-hmIDtfvS1dfGQ4Em5Prl6c2rulAuRVW7wbshgd79sMAsireyYYJnSFpqVnwKXgNwluUSp9wj0jUsT3r-JeueYunJNNvChC0zq6RmR2R4cIrYV6VhZq5QqjAG04S1GM_tVapvTLH_VhCkFhIaDrwJPu0gt4-uXcM2Ooo-wGoeYRqhFRH7WkrO7rbiH6rAAgzXLfIJdiNLQHffpSwWYxZ9wgKA0XTONFR2M2JhQ44nAae0xDCU4w66ML_5CRL2kMrmrtvBc/p.jpeg?fv_content=true&size_mode=5',
    'https://uc3bb3105e96b307b6e8af595264.previews.dropboxusercontent.com/p/thumb/AAYryib-9JHRSzDKOA542-KtBQwyzxF47AKx0-9YXfg7VRd21rCDhtF3aoevV6QQguBtVO4Ld_LA73R4nXD38w1bAvPxMzMCZKjLJOmlISFjLrqzB8EOLyLrDS0jp9QNS4QZeompNQlg2-CM3qZn3KfGH_fpi_Td5i8Lb8IPdazAfctxhyP-yxX5uXjwMCFNE-NnIWEw1TD_SHW1LceKZdQ4H-wRX4zckpBcFN4bNs7Ic3k9pk_lJUhq7x5hxvgfutUrWns1qrnDArRvrWigphTjU0bnBO7wZ1GLTfCoBq60BnLjCcKGEWo1M7CEAedULERnkd3Av8-BxA-ji6OtQ34_yiYwG58tV_aLKmS24Aghc-H3foGVf744JvoXgSJnOUI/p.jpeg?fv_content=true&size_mode=5');

$result = '<div id="lightgallery" class="rug-gallery">';
 foreach ($images as $img) {
     $result .= '<a href="'.$img.'"><img src="'.$img.'" /></a>';
 }
    $result .= '</div>';
    return $result;
}
add_shortcode('areagallery', 'arearugGallery');
//Tranding Product ShortCode

function shopofarearug($arg)
{
    $website_json =  json_decode(get_option('website_json'));
    $rugAffiliateCode = isset($website_json->rugAffiliateCode)?$website_json->rugAffiliateCode:"";
    $data = array(
        array(
            'url'=>'https://rugs.shop/area-rugs/rug-colors/?store='.$rugAffiliateCode,
            'image_url'=> plugins_url('../img/Color.png', __FILE__),
            'title'=> "Shop by Color"
        ),
        array(
            'url'=>'https://rugs.shop/area-rugs/rug-styles/?store='.$rugAffiliateCode,
            'image_url'=> plugins_url('../img/style.png', __FILE__),
            'title'=> "Shop by Style"
        ),
        array(
            'url'=>'https://rugs.shop/area-rugs/rug-sizes/?store='.$rugAffiliateCode,
            'image_url'=> plugins_url('../img/Size.png', __FILE__),
            'title'=> "Shop by Size"
        ),
        array(
            'url'=>'https://rugs.shop/area-rugs/rug-patterns/?store='.$rugAffiliateCode,
            'image_url'=> plugins_url('../img/patterns.png', __FILE__),
            'title'=> "Shop by Patterns"
        ),
        array(
            'url'=>'https://rugs.shop/area-rugs/rug-brands/?store='.$rugAffiliateCode,
            'image_url'=> plugins_url('../img/brands.png', __FILE__),
            'title'=> "Shop by Brands"
        ),
        array(
            'url'=>'https://rugs.shop/on-sale/?store='.$rugAffiliateCode,
            'image_url'=> plugins_url('../img/sale.png', __FILE__),
            'title'=> "Area Rug Sale"
        ),
       
    );
    $result ="";
    $result.= '<div class="products-list column-3"><ul class="product-list">';
    for ($i=0;$i<count($data);$i++) {
        $result .='<li class="product-li-three-row">
                    <div class="product-inner">
                        <div class="product-img-holder">
                            <a href="'.$data[$i]['url'].'" target="_blank" itemprop="url" class="">
                                <img src="'.$data[$i]['image_url'].'" alt="'.$data[$i]['title'].'" />
                            </a>
                        </div>
                        <h3 class="fl-callout-title"><a href="'.$data[$i]['url'].'" target="_blank">'.$data[$i]['title'].'</a></h3>
                    </div>
                </li>';
    }
    $result.= '</ul></div>';
    return $result;
}

add_shortcode('shoparearug', 'shopofarearug');

//Tranding Product ShortCode

function arearugProductList($arg)
{
    $website_json =  json_decode(get_option('website_json'));
    $rugAffiliateCode = isset($website_json->rugAffiliateCode)?$website_json->rugAffiliateCode:"";
    
    $data = array(
        array(
            'url'=>'https://rugs.shop/area-rugs/?store='.$rugAffiliateCode,
            'image_url'=>'https://mobilem.liquifire.com/mobilem?source=url[http://mmllc-images.s3.amazonaws.com/unitedweavers/3900201016320aqua.jpg]&scale=size[280x415]&sink',
            'price'=>'99.00',
            'title'=> "PETRA MULTI",
            'collection' => 'Spice Market',
            'brand' => 'Karastan'
        ),
        array(
            'url'=>'https://rugs.shop/area-rugs/?store='.$rugAffiliateCode,
            'image_url'=>'https://mobilem.liquifire.com/mobilem?source=url[http://mmllc-images.s3.amazonaws.com/unitedweavers/3900201016320aqua.jpg]&scale=size[280x415]&sink',
            'price'=>'499.00',
            'title'=> "Esperance Seaglass",
            'collection' => 'Titanium Collection',
            'brand' => 'Karastan'
        ),
        array(
            'url'=>'https://rugs.shop/area-rugs/?store='.$rugAffiliateCode,
            'image_url'=>'https://mobilem.liquifire.com/mobilem?source=url[http://mmllc-images.s3.amazonaws.com/unitedweavers/3900201016320aqua.jpg]&scale=size[280x415]&sink',
            'price'=>'156.00',
            'title'=> "Nirvana Indigo",
            'collection' => 'Cosmopolitan',
            'brand' => 'Karastan'
        ),
        array(
            'url'=>'https://rugs.shop/area-rugs/?store='.$rugAffiliateCode,
            'image_url'=>'https://mobilem.liquifire.com/mobilem?source=url[http://mmllc-images.s3.amazonaws.com/unitedweavers/3900201016320aqua.jpg]&scale=size[280x415]&sink',
            'price'=>'62.40',
            'title'=> "7139a",
            'collection' => 'Andora',
            'brand' => 'Oriental Weavers'
        )
    );
    $result ="";
    $result.= '<div class="products-list"><ul class="product-list">';
    for ($i=0;$i<count($data);$i++) {
        $result .='<li class="product-li">
            <div class="product-inner">
                <div class="product-img-holder">
                    <a href="'.$data[$i]['url'].'" target="_blank"><img src="'.$data[$i]['image_url'].'" alt="'.$data[$i]['title'].'" /></a>
                </div>
                <div class="product-info">
                    <h6>'.$data[$i]['collection'].'</h6>
                    <h4><a href="'.$data[$i]['url'].'" target="_blank">'.$data[$i]['title'].'</a></h4>

                    <div class="price-section">
                        <span>Starting at</span>
                        <span class="price"> $'.$data[$i]['price'].'</span>
                        <span class="sale">On Sale</span>
                    </div>

                    <div class="button-section">
                        <a href="'.$data[$i]['url'].'" class="button" target="_blank">Buy Now</a>
                        <div class="brand-wrap">By '.$data[$i]['brand'].'</div>
                    </div>
                </div>
            </div>
        </li>';
    }
    $result.= '</ul></div>';
    return $result;
}

add_shortcode('area_rug_trading_products', 'arearugProductList');



function formatPhoneNumber($phoneNumber)
{
    $phoneNumber = preg_replace('/[^0-9]/', '', $phoneNumber);

    if (strlen($phoneNumber) > 10) {
        $countryCode = substr($phoneNumber, 0, strlen($phoneNumber)-10);
        $areaCode = substr($phoneNumber, -10, 3);
        $nextThree = substr($phoneNumber, -7, 3);
        $lastFour = substr($phoneNumber, -4, 4);

        $phoneNumber = '+'.$countryCode.' ('.$areaCode.') '.$nextThree.'-'.$lastFour;
    } elseif (strlen($phoneNumber) == 10) {
        $areaCode = substr($phoneNumber, 0, 3);
        $nextThree = substr($phoneNumber, 3, 3);
        $lastFour = substr($phoneNumber, 6, 4);

        $phoneNumber = '('.$areaCode.') '.$nextThree.'-'.$lastFour;
    } elseif (strlen($phoneNumber) == 7) {
        $nextThree = substr($phoneNumber, 0, 3);
        $lastFour = substr($phoneNumber, 3, 4);

        $phoneNumber = $nextThree.'-'.$lastFour;
    }

    return $phoneNumber;
}

function storelocation_address($arg)
{
    
    $website = json_decode(get_option('website_json'));
    $weekdays = array("monday","tuesday","wednesday","thursday","friday","saturday","sunday");
    $locations = "<ul class='storename'><li>";
    
    
    for ($i=0;$i<count($website->locations);$i++) {
        $location_name = isset($website->locations[$i]->name)?$website->locations[$i]->name:"";

        if($website->locations[$i]->type == 'store'){

            if (in_array(trim($location_name), $arg)){

                $location_address_url =  isset($website->locations[$i]->name)?$website->locations[$i]->name." ":"";
                $location_address_url  .= isset($website->locations[$i]->address)?$website->locations[$i]->address." ":"";
                $location_address_url .= isset($website->locations[$i]->city)?$website->locations[$i]->city." ":"";
                $location_address_url .= isset($website->locations[$i]->state)?$website->locations[$i]->state." ":"";
                $location_address_url .= isset($website->locations[$i]->postalCode)?$website->locations[$i]->postalCode." ":"";
        
                $location_address  = isset($website->locations[$i]->address)?"<p>".$website->locations[$i]->address."</p>":"";
                $location_address .= isset($website->locations[$i]->city)?"<p>".$website->locations[$i]->city.", ":"<p>";
                $location_address .= isset($website->locations[$i]->state)?$website->locations[$i]->state:"";
                $location_address .= isset($website->locations[$i]->postalCode)?" ".$website->locations[$i]->postalCode."</p>":"</p>";
                
                $location_phone  = "";
                if (isset($website->locations[$i]->phone)) {
                    $location_tel = preg_replace('/[^0-9]/', '', $website->locations[$i]->phone);
                    $location_phone  = formatPhoneNumber($website->locations[$i]->phone);
                }
                if (isset($website->locations[$i]->forwardingPhone) && $website->locations[$i]->forwardingPhone != "") {
                    $forwarding_tel = preg_replace('/[^0-9]/', '', $website->locations[$i]->forwardingPhone);
                    $forwarding_phone  = formatPhoneNumber($website->locations[$i]->forwardingPhone);
                    
                }
                else{
        
                    $forwarding_tel ="#";
                    $forwarding_phone  = formatPhoneNumber(8888888888);
                }


                if (in_array("loc", $arg)) {

                    if (in_array('nolink', $arg)){
                        $locations .= '<div class="store-container">';
                        //$locations .= '<div class="name">'.$location_name.'</div>';
                        $locations .= '<div class="address">'.$location_address.'</div>';
                        //$locations .= "<div class='phone'><a href='tel:".$location_tel."'>"."<span>".$location_phone."</span></a></div>";
                        $locations .= '</div>';
                    }else{

                        $locations .= '<div class="store-container">';
                        //$locations .= '<div class="name">'.$location_name.'</div>';
                        $locations .= '<div class="address"><a href="https://maps.google.com/maps/?q='.urlencode($location_address_url).'" target="_blank" '.'>'.$location_address.'</a></div>';
                        //$locations .= "<div class='phone'><a href='tel:".$location_tel."'>"."<span>".$location_phone."</span></a></div>";
                        $locations .= '</div>';

                    }
                        
                    
                }
                if (in_array("forwardingphone", $arg)) {

                        if (in_array('nolink', $arg)){
                            $locations .= "<div class='phone'><span>".$forwarding_phone."</span></div>";
                        }
                        else{
                            $locations .= "<div class='phone'><a href='tel:".$forwarding_tel."'><span>".$forwarding_phone."</span></a></div>";
                        }

                }
                if (in_array("ohrs", $arg)) {
                        $locations .= '<div class="store-opening-hrs-container">';
                        $locations .= '<ul class="store-opening-hrs">';
                            
                        for ($j = 0; $j < count($weekdays); $j++) {
                            $location .= $website->locations[$i]->monday;
                            if (isset($website->locations[$i]->{$weekdays[$j]})) {
                                $locations .= '<li>'.ucfirst($weekdays[$j]).' : <span>'.$website->locations[$i]->{$weekdays[$j]}.'</span></li>';
                            }
                        }
                        $locations .= '</ul>';
                        $locations .= '</div>';
                }
                if (in_array("dir", $arg)) {
                    
                        $locations .= '<div class="direction">';
                        $locations .= '<a href="https://maps.google.com/maps/?q='.urlencode($location_address_url).'" target="_blank" class="fl-button" role="button">
                                            <span class="button-text">GET DIRECTIONS</span></a>';
                        $locations .= '</div>';
                    
                }
                if (in_array("map", $arg)) {
                    
                        $locations .= '<div class="map-container">
                        <iframe src="https://www.google.com/maps/embed/v1/place?key='.GOOGLE_MAP_KEY.'&amp;q='.urlencode($location_address_url).'"&zoom=nn style="width:100%;min-height:450px;border:0px;"></iframe>
                        </div>';
                    
                }
                if (in_array("phone", $arg)) {
                            $locations .= "<div class='phone'><a href='tel:".$location_tel."'>"."<span>".$location_phone."</span></a></div>";
                }
            } 

        }        
    }
$locations .= '</li>';
$locations .= '</ul>';
    
    return $locations;
}
    add_shortcode('storelocation_address', 'storelocation_address');


function contact_phonenumber($atts)
{
    if (get_option('api_contact_phone')) {
        return '<a href="tel:'.get_option('api_contact_phone').'" target="_self" class="fl-icon-text-link fl-icon-text-wrap">'.get_option('api_contact_phone').'</a>';
    } else {
        return '<a href="javascript:void(0)" target="_self" class="fl-icon-text-link fl-icon-text-wrap">(XXX) (XXXXXXX)</a>';
    }
}
add_shortcode('contactPhnumber', 'contact_phonenumber');


function getSocailIcons()
{
    $details = json_decode(get_option('social_links'));
    $return  = '';
    if (isset($details)) {
        $return  = '<ul class="social-icons">';
        foreach ($details as $key => $value) {
            if ($value->platform=="googleBusiness") {
                $value->platform = "google-plus";
            }
            if ($value->platform=="linkedIn") {
                $value->platform = "linkedin";
            }
            
                
                
            if ($value->active) {
                $return  .= '<li><a href="'.$value->url.'" target="_blank" title="'.$value->platform.'"><i class="fab fa-'.strtolower($value->platform).'"></i></a></li>';
            }
        }
        $return  .= '</ul>';
    }
    return $return;
}
add_shortcode('getSocailIcons', 'getSocailIcons');

function create_post_type()
{
    register_post_type(
        'store-locations',
        array(
        'labels' => array(
          'name' => __('Store Locations'),
          'singular_name' => __('Store Location')
        ),
        'public' => true,
        'has_archive' => true,
      )
    );
}
add_action('init', 'create_post_type');

function contactInformation($atts)
{
    $contacts = json_decode(get_option('website_json'));
    $info="";
    if (is_array($contacts->contacts)) {
        if (in_array("withicon", $atts)) {
            for ($i=0;$i<count($contacts->contacts);$i++) {
                if (in_array($contacts->contacts[$i]->type, $atts)) {
                    if (in_array("email", $atts)) {
                        $info  .= "<a class='mail-icon icon-link' href='mailto:".$contacts->contacts[$i]->email."'>".$contacts->contacts[$i]->email."</a>";
                    }
                    if (in_array("phone", $atts)) {
                        $info  .= "<a class='phone-icon icon-link' href='tel:".preg_replace('/[^0-9]/', '', $contacts->contacts[$i]->phone)."'>".formatPhoneNumber($contacts->contacts[$i]->phone)."</a>";
                    }
                    if (in_array("notes", $atts)) {
                        $info  .=  $contacts->contacts[$i]->notes;
                    }
                }
            }
        } else {
            for ($i=0;$i<count($contacts->contacts);$i++) {
                if (in_array($contacts->contacts[$i]->type, $atts)) {
                    if (in_array("email", $atts)) {
                        $info  .= "<a class='mail-icon-with icon-link-with' href='mailto:".$contacts->contacts[$i]->email."'>".$contacts->contacts[$i]->email."</a>";
                    }
                    if (in_array("phone", $atts)) {
                        $info  .= "<a class='phone-icon-with icon-link-with' href='tel:".preg_replace('/[^0-9]/', '', $contacts->contacts[$i]->phone)."'>".formatPhoneNumber($contacts->contacts[$i]->phone)."</a>";
                    }
                    if (in_array("notes", $atts)) {
                        $info  .=  $contacts->contacts[$i]->notes;
                    }
                }
            }
        }
    }
    return $info;
}
add_shortcode('contactInformation', 'contactInformation');


function locationInformation($atts)
{
    $website = json_decode(get_option('website_json'));
    
    for ($i=0;$i<count($website->locations);$i++) {

        if($website->locations[$i]->type == 'store'){

        if (in_array($website->locations[$i]->name, $atts)) {
            if (in_array("license", $atts)) {
                $info  .= "<div class='store-license'>".$website->locations[$i]->licenseNumber."</div>";
            }
            if (in_array("phone", $atts)) {
                $info  .= "<a class='phone-icon-with icon-link-with' href='tel:".preg_replace('/[^0-9]/', '', $website->locations[$i]->phone)."'>".formatPhoneNumber($website->locations[$i]->phone)."</a>";
            }
        }
    }
    }
    return $info;
}
add_shortcode('locationInformation', 'locationInformation');


function hookSharpStrings() {

    $website_json =  json_decode(get_option('website_json'));
    $sharpspringDomainId =$sharpspringTrackingId ="";
    for($j=0;$j<count($website_json->sites);$j++){
        
        if($website_json->sites[$j]->instance == ENV){
           
                $sharpspringTrackingId = $website_json->sites[$j]->sharpspringTrackingId;
                $sharpspringDomainId = $website_json->sites[$j]->sharpspringDomainId;
           
            }
        }
        
       if(isset($sharpspringTrackingId) && trim($sharpspringTrackingId) !="" ){
   
    ?>
           <script type="text/javascript">

                    var _ss = _ss || [];

                    _ss.push(['_setDomain', 'https://<?php echo $sharpspringDomainId;?>.marketingautomation.services/net']);

                    _ss.push(['_setAccount', '<?php echo $sharpspringTrackingId;?>']);

                    _ss.push(['_trackPageView']);

                    (function() {

                    var ss = document.createElement('script');

                    ss.type = 'text/javascript'; ss.async = true;

                    ss.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + '<?php echo $sharpspringDomainId;?>.marketingautomation.services/client/ss.js?ver=1.1.1';

                    var scr = document.getElementsByTagName('script')[0];

                    scr.parentNode.insertBefore(ss, scr);

                    })();

                    </script>
    <?php
       }
}
add_action('wp_head', 'hookSharpStrings');

function getRetailerInfo($arg){


    $website = json_decode(get_option('website_json'));
    if (in_array('city', $arg)){
        $info =  isset($website->locations[0]->city)?"<span class='city retailer'>".$website->locations[0]->city."</span>":"";
    }
    else if (in_array('state', $arg)){
        $info =  isset($website->locations[0]->state)?"<span class='state retailer'>".$website->locations[0]->state."</span>":"";
    }
    else if (in_array('zipcode', $arg)){
        $info =  isset($website->locations[0]->postalCode)?"<span class='zipcode retailer'>".$website->locations[0]->postalCode."</span>":"";
    }
    else if (in_array('legalname', $arg)){
        $info =  isset($website->name)?"<span class='name retailer'>".$website->name."</span>":"";
    }
    else if (in_array('address', $arg)){
        $info =  isset($website->locations[0]->address)?"<span class='street_address retailer'>".$website->locations[0]->address."</span>":"";
    }
    else if (in_array('phone', $arg)){
        if (isset($website->locations[0]->phone)) {
            $location_tel = preg_replace('/[^0-9]/', '', $website->locations[0]->phone);
            $location_phone  = formatPhoneNumber($website->locations[0]->phone);
        }
        if (in_array('nolink', $arg)){
            $info =  isset($website->locations[0]->phone)?"<span class='phone retailer'>".$location_phone."</span>":""; 
        }
        else{
            
            $info =  isset($website->locations[0]->phone)?"<a href='tel:".$location_tel."' class='phone retailer'>"."<span>".$location_phone."</span></a>":"";
        }
    }
    else if (in_array('forwarding_phone', $arg)){
        if (isset($website->locations[0]->forwardingPhone)) {
            $location_tel = preg_replace('/[^0-9]/', '', $website->locations[0]->forwardingPhone);
            $location_phone  = formatPhoneNumber($website->locations[0]->forwardingPhone);
        }
        if (in_array('nolink', $arg)){
            $info =  isset($website->locations[0]->forwardingPhone)?"<span class='phone retailer'>".$location_phone."</span>":""; 
        }
        else{
            
            $info =  isset($website->locations[0]->forwardingPhone)?"<a href='tel:".$location_tel."' class='phone retailer' >"."<span>".$location_phone."</span></a>":"";
        }
    }
    else if (in_array('companyname', $arg)){
        for($j=0;$j<count($website->sites);$j++){
        
            if($website->sites[$j]->instance == ENV){
                    $info =  isset($website->sites[$j]->name)?"".$website->sites[$j]->name."":"";
                    break;
                }
            }
    }
    else if (in_array('site_url', $arg)){
        for($j=0;$j<count($website->sites);$j++){
        
            if($website->sites[$j]->instance == ENV){
                    $info =  isset($website->sites[$j]->url)?"<span class='site_url retailer'>".$website->sites[$j]->url."</span>":"";
                    break;
                }
            }
    }
    
    return $info;  
}

add_shortcode('Retailer', 'getRetailerInfo');


function updateSaleCoupanInformation($arg){
    $promos = json_decode(get_option('promos_json'));
   // print_r($promos);
   $div='';

   $website_json =  json_decode(get_option('website_json'));
     $rugAffiliateCode = isset($website_json->rugAffiliateCode)?$website_json->rugAffiliateCode:"";
    
    
    $seleinformation = json_decode(get_option('saleinformation'));
    //update_option('saleinformation',json_encode($seleinformation));
    $seleinformation = (object)$seleinformation;
    
        if(in_array('rugshop',$arg) && in_array('salebanner',$arg)){
         

            foreach($promos as $promo){

               if($promo->rugShop == 1  && $promo->widgets){                

                foreach($promo->widgets as $banwidget){

                    if($banwidget->name =='Full Width Banner'){

                       
                       foreach($banwidget->images as $promoimg){

                        if($promoimg->type == 'mobile'){

                            $promomobile = $promoimg->url;

                       }
                       if($promoimg->type == 'desktop'){

                        $promodesk = $promoimg->url;

                       }

                        if(isset($promodesk) && $promodesk != ""){
                $div = "<div class='salebanner banner-deskop'><a href='https://rugs.shop/area-rugs/?store=".$rugAffiliateCode."'><img src='https://".$promodesk."' alt='sales' /></a></div>";
                         }
                          if(isset($promomobile) && $promomobile != ""){
                $div .= "<div class='salebanner banner-mobile'><a href='https://rugs.shop/area-rugs/?store=".$rugAffiliateCode."'><img src='https://".$promomobile."' alt='sales' /></a></div>";
                          }

                    }

                }
                

                }

            }
        }

        }else{
          
            $salebannerdata = json_decode(get_option('salebannerdata'));
          
      
       if(in_array('full',$arg) || in_array('salebanner',$arg)  ){  
         
        if($salebannerdata != ''){
       
            foreach($salebannerdata as $bannerinfo){
                
           
                if($bannerinfo->size == 'fullwidth'){  

                    $banner_img_deskop = $bannerinfo->images->desktop;
                    $banner_img_mobile = $bannerinfo->images->mobile;
                    $banner_link = $bannerinfo->link;
                }
            }
             
            $div = "<div class='salebanner banner-deskop fullwidth'><a href='".$banner_link."'><img srcset='https://mobilem.liquifire.com/mobilem?source=url[".$banner_img_deskop."]&scale=size[2880x200]&sink 4025w, 
            https://mobilem.liquifire.com/mobilem?source=url[".$banner_img_deskop."]&scale=size[2880x200]&sink 3019w, 
            https://mobilem.liquifire.com/mobilem?source=url[".$banner_img_deskop."]&scale=size[1583x110]&sink 2013w, 
            https://mobilem.liquifire.com/mobilem?source=url[".$banner_img_deskop."]&scale=size[1583x110]&sink 1006w' 
            
            src='https://mobilem.liquifire.com/mobilem?source=url[".$banner_img_deskop."]&scale=size[1583x110]&sink'  style='width:100%;text-align:center' alt='sales' />
            </a></div>";

       
        $div .= "<div class='salebanner banner-mobile'><a href='".$banner_link."'><img src='".$banner_img_mobile."' alt='sales' /></a></div>";
        }
          }
          if(in_array('fixed',$arg) && in_array('salebanner',$arg)){     
            if($salebannerdata != ''){  
           
            foreach($salebannerdata as $bannerinfo){

                if($bannerinfo->size == 'fixedwidth'){

                    $banner_img_deskop =  $bannerinfo->images->desktop;
                    $banner_img_mobile = $bannerinfo->images->mobile;
                    $banner_link = $bannerinfo->link;
                }
            }
        $div = "<div class='salebanner banner-deskop fixedwidth' style='text-align:center'><a href='".$banner_link."'><img src='https://mobilem.liquifire.com/mobilem?source=url[".$banner_img_deskop."]&scale=size[1583x110]&sink'  alt='sales' />
            </a></div>";

       
        $div .= "<div class='salebanner banner-mobile'><a href='".$banner_link."'><img src='".$banner_img_mobile."' alt='sales' /></a></div>";
          }
          }
        

        }
    if(in_array('categorybanner',$arg)){
     
        if(isset($seleinformation->category_banner_img_deskop)&& $seleinformation->category_banner_img_deskop != ""){ 
        $div = "<div class='salebanner ts banner-deskop'><a href='".$seleinformation->banner_link."'><img src='".$seleinformation->category_banner_img_deskop."' alt='sales' /></a></div>";        
        }
        if(isset($seleinformation->category_banner_img_mobile)&& $seleinformation->category_banner_img_mobile != "")
        $div .= "<div class='salebanner banner-mobile'><a href='".$seleinformation->banner_link."'><img src='".$seleinformation->category_banner_img_mobile."' alt='sales' /></a></div>";
    }
     if(in_array('heading',$arg)){
        $price = isset($seleinformation->saveupto_doller)?$seleinformation->saveupto_doller:"";
        if (@$_COOKIE['keyword'] ==""  && @$_COOKIE['brand'] == "") {
            
            $div ='<h1 class="googlekeyword">'.$price;
        } else {
            $keyword = $_COOKIE['keyword'];
            $brand = $_COOKIE['brand'];
            $div = '<h1 class="googlekeyword">'.$price.' on '.$brand.' '.$keyword.'<h1>';
        }
    }
     if(in_array('content',$arg)){
        
         $string = isset($seleinformation->content)?$seleinformation->content:"";
         $div = '<div class="form-content">'.$string.'</div>';
    }
    if(in_array('image_onform',$arg) && isset($seleinformation->image_onform)){
        
        if(in_array('backgrondimage',$arg) || in_array('background_img',$arg)){

            $div = "<div class='salebanner floor-coupon-img  banner-deskop'  id='backgrondimage' ><img src='".$seleinformation->image_onform."' alt='sales' /></div>";
            $div .= "<div class='salebanner floor-coupon-img  banner-mobile'  id='backgrondimage' ><img src='".$seleinformation->image_onform_mobile."' alt='sales' /></div>";
        }
        else    {
            $div = "<div class='salebanner floor-coupon-img  banner-deskop'><img src='".$seleinformation->image_onform."' alt='sales' /></div>";
            $div .= "<div class='salebanner floor-coupon-img  banner-mobile'><img src='".$seleinformation->image_onform_mobile."' alt='sales' /></div>";
        }
    }
    if(in_array('message',$arg) && isset($seleinformation->message)){
        
        $div = "<div class='message'>".$seleinformation->message."</div>";
       
    }
    if(in_array('navigation',$arg) && isset($seleinformation->navigation_img)){
        if (in_array('image', $arg)) {
            $div = "<div class='navigation_img'><a href='".$seleinformation->navigation_link."'><img src='".$seleinformation->navigation_img."' alt='sales' /></a></div>";
        }
        else if(in_array('text',$arg)){
            $div = "<div class='navigation_img'><a href='".$seleinformation->navigation_link."'>'".$seleinformation->navigation_text."</a></div>";
        }
       
    }
    if (in_array('homepage_banner', $arg)) {
        if (isset($seleinformation->slider) && count($seleinformation->slider) > 0) {
            foreach ($seleinformation->slider as $slide) {
                $div = "<div class='floor-coupon-img' id='homepage-banner-bg' data-bg='".$seleinformation->background_image."'><a href='/flooring-coupon/'><img class='salebanner-slide' src='".$slide->slider_coupan_img."' alt='sales' /></a></div>";
				break;
            }
        }
    }
	if (in_array('banner', $arg)) {
        if (isset($seleinformation->slider) && count($seleinformation->slider) > 0) {
            foreach ($seleinformation->slider as $slide) {
                $div = "<div class='banner-image' style=''><a href='/flooring-coupon/'><img class='salebanner-slide' src='".$slide->slider_coupan_img."' alt='sales' /></a></div>";
				break;
            }
        }
    }
    
    
    
    return $div;
}

add_shortcode('coupon', 'updateSaleCoupanInformation');

function copyrightstext($arg)
{
    $result = "<p>Copyright ©".do_shortcode('[fl_year]')." ".get_bloginfo()." All Rights Reserved.</p>";
    return $result;
}

add_shortcode('copyrights', 'copyrightstext');


//Colorwall Home Page Banner Shortcode

function colorwall_home_banner($arg)
{
    $product_json =  json_decode(get_option('product_json')); 
 
    foreach ($product_json as $data) {
        foreach ($data as $key => $value) {
           if( $key=="manufacturer" &&   $value== "coretec")
               $isCoretec=true;
        }     
    }
    $coretec_colorwall =  get_option('coretec_colorwall');
    if($coretec_colorwall == '1' && $isCoretec){

       $content = do_shortcode('[fl_builder_insert_layout slug="colorwall-mobile"]');
       $content .=  do_shortcode('[fl_builder_insert_layout slug="colorwall-desktop-banner"]');

       return $content;

    }
}

add_shortcode('colorwallhomebanner', 'colorwall_home_banner');


//Yoast SEO Breadcrumb addded
function bbtheme_yoast_breadcrumbs() {
    if ( function_exists('yoast_breadcrumb') && ! is_front_page() ) {
        yoast_breadcrumb('<div id="breadcrumbs"><div class="container">','</div></div>');
    }
}
add_action( 'fl_content_open', 'bbtheme_yoast_breadcrumbs' );

//Yoast SEO Breadcrumb link - Changes for PDP pages
add_filter( 'wpseo_breadcrumb_links', 'wpse_100012_override_yoast_breadcrumb_trail' );

function wpse_100012_override_yoast_breadcrumb_trail( $links ) {
    global $post;

    if ( is_singular( 'hardwood_catalog' )  ) {
        $breadcrumb[] = array(
            'url' => get_site_url().'/flooring',
            'text' => 'Flooring',
        );
        $breadcrumb[] = array(
            'url' => get_site_url().'/flooring/hardwood',
            'text' => 'Hardwood',
        );
        $breadcrumb[] = array(
            'url' => get_site_url().'/flooring/hardwood/products',
            'text' => 'Products',
        );
        array_splice( $links, 1, -2, $breadcrumb );

    }elseif (is_singular( 'carpeting' )) {
        $breadcrumb[] = array(
            'url' => get_site_url().'/flooring',
            'text' => 'Flooring',
        );
        $breadcrumb[] = array(
            'url' => get_site_url().'/flooring/carpet',
            'text' => 'Carpet',
        );
        $breadcrumb[] = array(
            'url' => get_site_url().'/flooring/carpet/products',
            'text' => 'Products',
        );
        array_splice( $links, 1, -2, $breadcrumb );

    }elseif (is_singular( 'luxury_vinyl_tile' )) {

        $breadcrumb[] = array(
            'url' => get_site_url().'/flooring',
            'text' => 'Flooring',
        );
        $breadcrumb[] = array(
            'url' => get_site_url().'/flooring/vinyl',
            'text' => 'Vinyl',
        );
        $breadcrumb[] = array(
            'url' => get_site_url().'/flooring/vinyl/products',
            'text' => 'Products',
        );
        array_splice( $links, 1, -2, $breadcrumb );
        
    }elseif (is_singular( 'laminate_catalog' )) {

        $breadcrumb[] = array(
            'url' => get_site_url().'/flooring',
            'text' => 'Flooring',
        );
        $breadcrumb[] = array(
            'url' => get_site_url().'/flooring/laminate',
            'text' => 'Laminate',
        );
        $breadcrumb[] = array(
            'url' => get_site_url().'/flooring/laminate/products',
            'text' => 'Products',
        );
        array_splice( $links, 1, -2, $breadcrumb );
        
    }elseif (is_singular( 'tile_catalog' )) {

        $breadcrumb[] = array(
            'url' => get_site_url().'/flooring',
            'text' => 'Flooring',
        );
        $breadcrumb[] = array(
            'url' => get_site_url().'/flooring/tile',
            'text' => 'Tile',
        );
        $breadcrumb[] = array(
            'url' => get_site_url().'/flooring/tile/products',
            'text' => 'Products',
        );
        array_splice( $links, 1, -2, $breadcrumb );
        
    }elseif (is_singular( 'solid_wpc_waterproof' )) {

        $breadcrumb[] = array(
            'url' => get_site_url().'/flooring',
            'text' => 'Flooring',
        );
        $breadcrumb[] = array(
            'url' => get_site_url().'/flooring/waterproof-flooring',
            'text' => 'Waterproof Flooring',
        );
        $breadcrumb[] = array(
            'url' => get_site_url().'/flooring/waterproof-flooring/products',
            'text' => 'Products',
        );
        array_splice( $links, 1, -2, $breadcrumb );
    }

    return $links;
}


  //This filter is executed before displaying each field and can be used to dynamically populate fields with a default value.
  
  add_filter( 'gform_field_value_salepromocode', 'populate_salepromocode' );
  function populate_salepromocode( $value ) {
    $sale_json =  json_decode(get_option('saleconfiginformation'));
    $forms = GFAPI::get_forms();

    foreach( $sale_json as $sale){
        if( $sale->isRugShop == ''){
           
            foreach($forms as $form){

                if($form['title'] == $sale->formName){

                    $salecode = $sale->promoCode;

                }

            }
            
        }
    }
     return $salecode;
  } 

  add_filter( 'gform_field_value_saleformtype', 'populate_saleformtype' );
  function populate_saleformtype( $value ) {
    $sale_json =  json_decode(get_option('saleconfiginformation'));
    $forms = GFAPI::get_forms();

    foreach( $sale_json as $sale){
        if( $sale->isRugShop == ''){
           
            foreach($forms as $form){

                if($form['title'] == $sale->formName){

                    $saleformtype = $sale->formType;

                }

            }
            
        }
    }
     return $saleformtype;
  } 


   
// Add Yoast SEO sitemap to virtual robots.txt file
function surbma_yoast_seo_sitemap_to_robotstxt_function( $output ) {
	$options = get_option( 'wpseo_xml' );
	if ( class_exists( 'WPSEO_Sitemaps' ) && $options['enablexmlsitemap'] == true ) {
		$homeURL = get_home_url();
    	$output .= "Sitemap: $homeURL/sitemap_index.xml\n";
	}
	return $output;
}
add_filter( 'robots_txt', 'surbma_yoast_seo_sitemap_to_robotstxt_function', 9999, 1 );

//Floorte Home Page Banner Shortcode

function floorte_home_banner($arg)
{   
       $content = do_shortcode('[fl_builder_insert_layout slug="floorte-mobile"]');
       $content .=  do_shortcode('[fl_builder_insert_layout slug="floorte-desktop"]');

       return $content;
}

add_shortcode('floortehomebanner', 'floorte_home_banner');


function featured_products_slider($atts) {

    // print_r($atts);
 
     $args = array(
         'post_type' => $atts['0'],        
         'posts_per_page' => 8,
         'orderby' => 'rand',
         'meta_query' => array(
             array(
                 'key' => 'collection', 
                 'value' => $atts['1'], 
                 'compare' => '=='
             )
         ) 
     );
     $query = new WP_Query( $args );
     if( $query->have_posts() ){
         $outpout = '<div class="featured-products"><div class="featured-product-list">';
         while ($query->have_posts()) : $query->the_post(); 
             $gallery_images = get_field('gallery_room_images');
             $gallery_img = explode("|",$gallery_images);
             $count = 0;
             // loop through the rows of data
             foreach($gallery_img as  $key=>$value) {
                 $room_image = $value;
                 if(!$room_image){
                     $room_image = "http://placehold.it/245x310?text=No+Image";
                 }
                 else{
                     if(strpos($room_image , 's7.shawimg.com') !== false){
                         if(strpos($room_image , 'http') === false){ 
                             $room_image = "http://" . $room_image;
                         }
                         $room_image = $room_image ;
                     } else{
                         if(strpos($room_image , 'http') === false){ 
                             $room_image = "https://" . $room_image;
                         }
                         $room_image= "https://mobilem.liquifire.com/mobilem?source=url[".$room_image . "]&scale=size[524x636]&sink";
                     }
                 }
                 if($count>0){
                     break;
                 }
                 $count++;
             }
 
             $color_name = get_field('color');
             $brand_name = get_field('brand');
             $collection = get_field('collection');
             $post_type = get_post_type_object(get_post_type( get_the_ID() ));
             
             $outpout .= '<div class="featured-product-item">
                 <div class="featured-inner">
                     <div class="prod-img-wrap">
                         <img src="'.$room_image.'" alt="'.get_the_title().'" />
                         <div class="button-wrapper">
                             <div class="button-wrapper-inner">
                                 <a href="'.get_the_permalink().'" class="button alt">View Product</a>
                                 <a href="/flooring-coupon/" class="button">Get Coupon</a>
                             </div>
                         </div>
                     </div>
                     <div class="product-info">
                         <div class="slider-product-title"><a href="'.get_the_permalink().'">'.$collection.'</a></div>
                         <h3 class="floorte-color">'.$color_name.'</h3>
                     </div>
                 </div>
             </div>';
 
         endwhile;
         $outpout .= '</div></div>';
         wp_reset_query();
     }  
 
 
     return $outpout;
 }
 add_shortcode( 'featured_products_slider', 'featured_products_slider' );

 //Brand Banner header Shortcode

function brand_header_banner_function($arg)
{ 
    $storename = do_shortcode('[Retailer "companyname"]');

    if(in_array('floorte', $arg)){

        $content = '<img src="https://mmllc-images.s3.us-east-2.amazonaws.com/floorte/floorte-header.jpg" alt="Floorte waterproof hardwood flooring for home | '.$storename.'" title="" itemprop="image"  />';
    }
   

       return $content;

  
}

add_shortcode('brand_header_banner', 'brand_header_banner_function');

function brand_roomscene_function($arg)
{ 
    $storename = do_shortcode('[Retailer "companyname"]');

    if(in_array('floorte', $arg)){

        $content = '<img src="https://mmllc-images.s3.us-east-2.amazonaws.com/floorte/roomscenes.jpg" alt="Floorte waterproof hardwood flooring for your home | '.$storename.'" title="" itemprop="image"  />';
    }
   

       return $content;

  
}

add_shortcode('brand_roomscene', 'brand_roomscene_function');

function brand_contact_function($arg)
{ 
    $storename = do_shortcode('[Retailer "companyname"]');

    if(in_array('floorte', $arg)){

        $content = '<img src="https://mmllc-images.s3.us-east-2.amazonaws.com/floorte/Floorte-Hardwood-2-tier-Rendering_FINAL.jpg" alt="Floorte waterproof hardwood flooring display | '.$storename.'" title="" itemprop="image"  />';
    }
   

       return $content;

  
}

add_shortcode('brand_contact', 'brand_contact_function');


function brand_construction_function($arg)
{ 
    $storename = do_shortcode('[Retailer "companyname"]');

    if(in_array('floorte', $arg)){

        $content = '<img src="https://mmllc-images.s3.us-east-2.amazonaws.com/floorte/Group-6.jpg" alt="Floorte waterproof hardwood flooring construction | '.$storename.'" title="" itemprop="image"  />';
    }
   

       return $content;

  
}
add_shortcode('brand_construction', 'brand_construction_function');

function brand_construction_mobile_function($arg)
{ 
    $storename = do_shortcode('[Retailer "companyname"]');

    if(in_array('floorte', $arg)){

        $content = '<img src="https://mmllc-images.s3.us-east-2.amazonaws.com/floorte/floorte-layers-mobile.jpg" alt="Floorte waterproof hardwood flooring construction | '.$storename.'" title="" itemprop="image"  />';
    }
   

       return $content;

  
}

add_shortcode('brand_construction_mobile', 'brand_construction_mobile_function');

function brand_resistant_function($arg)
{ 
    $storename = do_shortcode('[Retailer "companyname"]');

    if(in_array('floorte', $arg)){

        $content = '<img src="https://mmllc-images.s3.us-east-2.amazonaws.com/floorte/floorte-scuf-pic.jpg" alt="Floorte waterproof hardwood flooring with stain resistant finish | '.$storename.'" title="" itemprop="image"  />';
    }
   

       return $content;

  
}

add_shortcode('brand_resistant', 'brand_resistant_function');