<div class="product-grid swatch" itemscope itemtype="http://schema.org/CollectionPage">
    <div class="row product-row">
    <?php 
  
    $show_financing = get_option('sh_get_finance');

    if(postpercol == '4')
    {
        $col_class = 'col-md-3 col-sm-4 col-xs-6';
    }
    else
    {
        $col_class = 'col-md-4 col-sm-4';
    }

    $col_class = 'col-md-3 col-sm-4 col-xs-6';
    ?>
<?php 
if(have_posts()){
while ( have_posts() ): the_post(); 
      //collection field
      global $post;
       $flooringtype = $post->post_type; 
      $collection = get_field('collection', $post->ID);
      $in_stock = get_field('in_stock', $post->ID);
?>
    <div class="<?php echo $col_class; ?>">
    
    <div class="fl-post-grid-post" itemscope itemtype="Product">
        <?php FLPostGridModule::schema_meta(); ?>
        <h2 class="fl-post-grid-title" itemprop="headline">
                <a href="<?php the_permalink(); ?>" class="in_stock_title" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a>
            </h2>
        <?php if(get_field('swatch_image_link')) { ?>
            <div class="fl-post-grid-image">
                <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
                    <?php //the_post_thumbnail($settings->image_size); ?>
  				<?php 
												
				     $itemImage = get_field('swatch_image_link');

					 if(strpos($itemImage , 's7.shawimg.com') !== false){
					        if(strpos($itemImage , 'http') === false){ 
							  $itemImage = "http://" . $itemImage;
							}	
						 $class = "";
					}else{
						   if(strpos($itemImage , 'http') === false){ 
								$itemImage = "https://" . $itemImage;
							}	
						 $class = "shadow";
					}	
					$image= "https://mobilem.liquifire.com/mobilem?source=url[".$itemImage ."]&scale=size[225]&sink";
							
							
					?>
            <img class="<?php echo $class; ?>" src="<?php  echo $image; ?>" alt="<?php the_title_attribute(); ?>" />
            
            <?php
            // exclusive icon condition
            if($in_stock == '1' || $in_stock == '1') {    ?>
			<!-- <span class="exlusive-badge in_stock_badge"><img src="<?php echo plugins_url( '/product-listing-templates/images/exclusive-icon.png', dirname(__FILE__) );?>" alt="<?php the_title(); ?>" /></span> -->
			<?php } ?>
                  
                </a>
            </div>
        <?php } else { ?>
            <div class="fl-post-grid-image">
                <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
                    <?php //the_post_thumbnail($settings->image_size); ?>
                    <img src="http://placehold.it/225x225?text=No+Image" alt="<?php the_title_attribute(); ?>" />
                </a>
            </div>

        <?php } ?>
        <div class="fl-post-grid-text product-grid btn-grey">
            <h4 class="in_stock_label"><strong>IN - STOCK</strong></h4>

            <?php 
           
                $args = array(
                    'post_type'      => $flooringtype,
                    'posts_per_page' => -1,
                    'post_status'    => 'publish',
                    'meta_query'     => array(
                        array(
                            'key'     => 'collection',
                            'value'   => $collection,
                            'compare' => '='
                        )
                    )
                );
            
                $in_stock_query = new WP_Query( $args );
            ?>

            <h4 class="in_stock_count"> <?php echo $in_stock_query->found_posts;?> COLORS AVAILABLE</h4>
            <a href="<?php echo site_url(); ?>/contact-us/" target="_self" class="fl-button in_stock_contact_btn" role="button" <?php //get_coupon_button_visibility($sale_arr,$brand_arr); ?> >
                <span class="fl-button-text">CONTACT US</span>
            </a>
        </div>
    </div>
    </div>
<?php endwhile; 
}else{ ?>
    <h2>No Products available</h2>
<?php }
?>
</div>
</div>