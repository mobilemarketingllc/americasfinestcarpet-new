<div class="product-grid swatch" itemscope itemtype="http://schema.org/CollectionPage">
    <div class="row product-row">
    <?php 

    $show_financing = get_option('sh_get_finance');

    if(postpercol == '4')
    {
        $col_class = 'col-md-3 col-sm-4 col-xs-6';
    }
    else
    {
        $col_class = 'col-md-4 col-sm-4';
    }
    ?>
<?php while ( have_posts() ): the_post(); 
      //collection field
      $collection = get_field('collection', $post->ID);
?>
    <div class="<?php echo $col_class; ?>">    
    <div class="fl-post-grid-post" itemscope itemtype="Product">
        <?php FLPostGridModule::schema_meta(); ?>
        <?php if(get_field('swatch_image_link')) { ?>
            <div class="fl-post-grid-image">
                <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
                    <?php //the_post_thumbnail($settings->image_size); ?>
  				<?php 
												
				     $itemImage = get_field('swatch_image_link');

					 if(strpos($itemImage , 's7.shawimg.com') !== false){
					        if(strpos($itemImage , 'http') === false){ 
							  $itemImage = "http://" . $itemImage;
							}	
						 $class = "";
					}else{
						   if(strpos($itemImage , 'http') === false){ 
								$itemImage = "https://" . $itemImage;
							}	
						 $class = "shadow";
					}	
					$image= "https://mobilem.liquifire.com/mobilem?source=url[".$itemImage ."]&scale=size[222]&sink";
							
							
					?>
            <img class="<?php echo $class; ?>" src="<?php  echo $image; ?>" alt="<?php the_title_attribute(); ?>" />
            
            <?php
            // exclusive icon condition
            if($collection == 'COREtec Colorwall' || $collection == 'Coretec Colorwall' ||  $collection == 'Floorte Magnificent') {    ?>
			<span class="exlusive-badge"><img src="<?php echo plugins_url( '/product-listing-templates/images/exclusive-icon.png', dirname(__FILE__) );?>" alt="<?php the_title(); ?>" /></span>
			<?php } ?>
                  
                </a>
            </div>
        <?php } else { ?>
            <div class="fl-post-grid-image">
                <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
                    <?php //the_post_thumbnail($settings->image_size); ?>
                    <img src="http://placehold.it/300x300?text=No+Image" alt="<?php the_title_attribute(); ?>" />
                </a>
            </div>

        <?php } ?>
        <div class="fl-post-grid-text product-grid btn-grey">
            <h4><?php if($collection == 'COREtec Colorwall' || $collection == 'Coretec Colorwall') { ?> <?php the_field('collection'); ?> <?php the_field('style'); ?> <?php } else{ ?><?php the_field('collection'); ?> <?php } ?> </h4>
            <h2 class="fl-post-grid-title" itemprop="headline">
                <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><?php if($collection == 'COREtec Colorwall' || $collection == 'Coretec Colorwall') { echo get_field('color'); } else{ echo get_field('color'); }?></a>
            </h2>

            <?php 

             
            
             $promsale =  json_decode(get_option('saleconfiginformation')); 

             //print_r($promsale)

             $sale_arr = array();

             $brand_arr = array();

             $i = 0 ;



             foreach ($promsale as $sale) {
                
                 if($sale->getCoupon == 1){

                    $brand_arr = array_merge($brand_arr,$sale->brandList);

                    $sale_end_date   =  date("d-m-Y", substr($sale->endDate, 0, 10)); 
                    $sale_start_date =  date("d-m-Y", substr($sale->startDate, 0, 10)); 

                    $sale_arr[$i]['promoCode'] = $sale->promoCode;
                    $sale_arr[$i]['name']      = $sale->name; 
                    $sale_arr[$i]['startDate'] = $sale_start_date; 
                    $sale_arr[$i]['endDate']   = $sale_end_date;   
                    $sale_arr[$i]['getCoupon'] = $sale->getCoupon;   
                    $sale_arr[$i]['brandList'] = $sale->brandList;   


                    $i++;
                 }
             }
            // echo '<pre>';
            // print_r($promsale);
            // print_r($sale_arr);
            //  echo '</pre>';
         //echo    get_field('brand', get_the_id());
            ?>

            <a href="<?php echo site_url(); ?>/flooring-coupon/" target="_self" class="fl-button plp-getcoupon-btn" role="button" <?php //get_coupon_button_visibility($sale_arr,$brand_arr); ?> >
                <span class="fl-button-text">GET COUPON</span>
            </a>
           <?php if($show_financing == 1){?>
            <a href="<?php echo site_url(); ?>/flooring-financing/" target="_self" class="fl-button plp-getfinance-btn" role="button" >
                <span class="fl-button-text">GET FINANCING</span>
            </a>
           <?php } ?>
            <br />
            
            <a class="link" href="<?php the_permalink(); ?>">VIEW PRODUCT</a>
        </div>
    </div>
    </div>
<?php endwhile; ?>
</div>
</div>